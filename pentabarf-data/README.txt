This data is data extracted from Pentabarf.  It was constantly updated
before and during the conference for our planning information.  This
dump is from 1 August, right after DebConf.

This is what Pentabarf says, but of course may not reflect reality,
since Pentabarf isn't always updated.

In particular, all the budget numbers are estimates and shouldn't be
taken as meaning that much.

There are many places where it divides among the different sponsorship
types:
B - basic (no sponsorship)
S/BAF - accom and food sponsorship (Sponsored) category
P/PRO - professional
C/COR - corporate

