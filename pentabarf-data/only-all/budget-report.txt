Mon Aug  1 10:11:31 2011

====================
Statistics for all people
Number of people:      656
Total nights:          5427

Total room-nights:     3100
Total room cost:       0
Total food-days:       4507
Total food cost:       0

Total cost:            0

Fees, professional:    24870.0
Fees, corporate:       2000
Fees, other:           0

Total fees:            26870.0

Net cost of attendees: -26870.0



====================
Statistics for sponsored people (BAF, BA, BF)
Number of people:      280
Total nights:          2580

Total room-nights:     2322
Total room cost:       0
Total food-days:       2564
Total food cost:       0

Total cost:            0

Fees, professional:    0
Fees, corporate:       0
Fees, other:           0

Total fees:            0

Net cost of attendees: 0



====================
Statistics for unsponsored people (CORP, PROF)
Number of people:      54
Total nights:          434

Total room-nights:     300
Total room cost:       0
Total food-days:       426
Total food cost:       0

Total cost:            0

Fees, professional:    24870.0
Fees, corporate:       2000
Fees, other:           0

Total fees:            26870.0

Net cost of attendees: -26870.0



====================
Statistics for other people !(B, BAF, BA, BF, CORP, PROF)
Number of people:      322
Total nights:          2413

Total room-nights:     478
Total room cost:       0
Total food-days:       1517
Total food cost:       0

Total cost:            0

Fees, professional:    0
Fees, corporate:       0
Fees, other:           0

Total fees:            0

Net cost of attendees: 0



