Mon Aug  1 10:11:26 2011

Assuming anyone without an arrival date arrives on 2011-07-23 (or 2011-07-17
for DebCamp) and anyone without a departure date leaves on 2011-07-31.


====================
Statistics for all people
Number of people: 656
Number of people with accom: 294
Number of people without accom: 362
Total person-room-days: 3100

                  Room   OwnAccom
2011-07-12(Tue)     0        0
2011-07-13(Wed)     1        0
2011-07-14(Thu)     1        0
2011-07-15(Fri)     1        0
2011-07-16(Sat)     3        0
2011-07-17(Sun)    47       13
2011-07-18(Mon)    62       13
2011-07-19(Tue)    70       14
2011-07-20(Wed)    80       14
2011-07-21(Thu)    90       14
2011-07-22(Fri)   107       15
2011-07-23(Sat)   230      304
2011-07-24(Sun)   287      343
2011-07-25(Mon)   289      348
2011-07-26(Tue)   289      348
2011-07-27(Wed)   290      351
2011-07-28(Thu)   287      349
2011-07-29(Fri)   283      344
2011-07-30(Sat)   242      314
2011-07-31(Sun)     0        0



====================
Statistics for ONLY people who have both dates in Penta
Number of people: 342
Number of people with accom: 262
Number of people without accom: 80
Total person-room-days: 2408

                  Room   OwnAccom
2011-07-12(Tue)     0        0
2011-07-13(Wed)     1        0
2011-07-14(Thu)     1        0
2011-07-15(Fri)     1        0
2011-07-16(Sat)     3        0
2011-07-17(Sun)    45        5
2011-07-18(Mon)    60        5
2011-07-19(Tue)    68        6
2011-07-20(Wed)    78        6
2011-07-21(Thu)    88        6
2011-07-22(Fri)   105        7
2011-07-23(Sat)   198       27
2011-07-24(Sun)   255       65
2011-07-25(Mon)   257       68
2011-07-26(Tue)   257       67
2011-07-27(Wed)   258       70
2011-07-28(Thu)   255       68
2011-07-29(Fri)   251       63
2011-07-30(Sat)   210       33
2011-07-31(Sun)     0        0



====================
Statistics for (accom) sponsored people (S)
Number of people: 280
Number of people with accom: 248
Number of people without accom: 32
Total person-room-days: 2322

                  Room   OwnAccom
2011-07-12(Tue)     0        0
2011-07-13(Wed)     1        0
2011-07-14(Thu)     1        0
2011-07-15(Fri)     1        0
2011-07-16(Sat)     3        0
2011-07-17(Sun)    42        6
2011-07-18(Mon)    57        6
2011-07-19(Tue)    64        7
2011-07-20(Wed)    73        7
2011-07-21(Thu)    83        7
2011-07-22(Fri)    98        7
2011-07-23(Sat)   202       27
2011-07-24(Sun)   246       31
2011-07-25(Mon)   246       31
2011-07-26(Tue)   246       31
2011-07-27(Wed)   246       32
2011-07-28(Thu)   243       32
2011-07-29(Fri)   240       31
2011-07-30(Sat)   205       28
2011-07-31(Sun)     0        0



====================
Statistics for UNsponsored people (!S)
Number of people: 376
Number of people with accom: 46
Number of people without accom: 330
Total person-room-days: 778

                  Room   OwnAccom
2011-07-16(Sat)     0        0
2011-07-17(Sun)     5        7
2011-07-18(Mon)     5        7
2011-07-19(Tue)     6        7
2011-07-20(Wed)     7        7
2011-07-21(Thu)     7        7
2011-07-22(Fri)     9        8
2011-07-23(Sat)    28      277
2011-07-24(Sun)    41      312
2011-07-25(Mon)    43      317
2011-07-26(Tue)    43      317
2011-07-27(Wed)    44      319
2011-07-28(Thu)    44      317
2011-07-29(Fri)    43      313
2011-07-30(Sat)    37      286
2011-07-31(Sun)     0        0



