Total events submitted:
203


====================
Event types:
lecture             63
bof                 54
meeting             30
other               17
workshop            17
lightning            8
podium               5
contest              3
None                 0


====================
Event states:
accepted           174
undecided           17
rejected            12


====================
Event states and progress:
accepted           canceled            10
accepted           confirmed          159
accepted           unconfirmed          5
rejected           unconfirmed         12
undecided          new                 17


====================
Number of talks per person:
10  1
 9  0
 8  0
 7  0
 6  5
 5  4
 4  3
 3  9
 2 24
 1 81


====================
Number of talks by each person type:
Debian Developer                    149
Otherwise involved in Debian         26
Not yet involved but interested      21
DebConf Organizer                    17
DebConf Volunteer                     8
Sponsor                               2
Accompanying a Debian participant     2
Press                                 1


====================
Number of talks, by DebCamp attendance
I won't be attending DebCamp             226
