[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Sponsorship"
  lang = "en"
%]
<h2>Sponsorship: Opportunities and benefits</h2>

<h3>Why?</h3>

<p align="justify">Debian is a huge fast-growing community, with developers and users from all over the world, while Debian operating system is universal and free in every sense of the word, used on countless computers, machines and platforms. As such, Debian user community is a world-wide phenomenon and a great opportunity for you to promote your business and your company, allowing our community to develop even further and serve larger audience!</p>

<p align="justify">Within the Debian community there is a strong sense of loyalty and brand awareness. Sponsoring a large conference is a good method of ingratiating your brand with the attendees, enabling you to build up your company's image. Doing so a meaningful relationship will be built, providing you with opportunites for not only advertising, but contact building and even recruiting, proven by previous DebConfs.</p>

<h3>More</h3>

<p align="justify">If you are interested in finding out more about sponsorship, requirements, opportunities and benefits, please read our <b><a href="http://debconf11.debconf.org/sponsorship/dc11_brochure_en_hq.pdf">sponsorship brochure</a></b>. Should you have any questions, feedbacks or you have already decided to be a sponsor, please contact us trough following email address:</p>

<p align="center"><b><a href="mailto:sponsors@debconf.org" alt="Sponsorship Team mail address">sponsors@debconf.org</a></b></p>

<p align="justify">If you already chose to pay/donate, you can find instructions and bank account information, please visit the <b><a href="/payments.xhtml.en">Payments & donations</a></b>.

<h3>Sponsorship levels</h3>

<p align="justify">DebConf sponsors are classified according to the amount of money contributed to the organization of the conference. Each sponsorship level is defined by donation levels and by in-return benefits of sponsorship and appreciation for the sponsorship received. If a sponsor contributes with hardware or other non-monetary donation, it will be taken into account for the categorization.</p>

<h4>STEEL - 1.000 to 2.000 &euro;</h4>
<ul>
  <li>Logo on our website</li>
  <li>Logo included in full-page thank you ad in Linux Magazine worldwide</li>
  <li>Logo printed on attendee bag. You can also send promotional material (leaflets or branded merchandise) to be included in the bags.</li>
</ul>

<h4>BRONZE - 2.000 to 5.000 &euro;</h4>
<ul>
  <li>Logo on our website</li>
  <li>Logo included in full-page thank you ad in Linux Magazine worldwide</li>
  <li>Logo printed on attendee bag. You can also send promotional material (leaflets or branded merchandise) to be included in the bags.</li>
  <li>Logo printed on t-shirts</li>
</ul>

<h4>SILVER - 5.000 to 10.000 &euro;</h4>
<ul>
  <li>Logo on our website </li>
  <li>Logo included in full-page thank you ad in Linux Magazine worldwide</li>
  <li>Logo printed on attendee bag. You can also send promotional material (leaflets or branded merchandise) to be included in the bags.</li>
  <li>Logo printed on t-shirts</li>
  <li>Logo on all video transmissions</li>
</ul>

<h4>GOLD - 10.000 to 25.000 &euro;</h4>
<ul>
  <li>Logo on our website </li>
  <li>Logo included in full-page thank you ad in Linux Magazine worldwide</li>
  <li>Logo printed on attendee bag. You can also send promotional material (leaflets or branded merchandise) to be included in the bags.</li>
  <li>Logo printed on t-shirts</li>
  <li>Logo on all video transmissions</li>
  <li>Logo on banner in conference lobby and behind talk podiums</li>
</ul>

<h4>PLATINUM - 25.000 &euro; or more</h4>
<ul>
  <li>Logo on our website </li>
  <li>Logo included in full-page thank you ad in Linux Magazine worldwide</li>
  <li>Logo printed on attendee bag. You can also send promotional material (leaflets or branded merchandise) to be included in the bags.</li>
  <li>Logo printed on t-shirts</li>
  <li>Logo on all video transmissions</li>
  <li>Logo on banner in conference lobby and behind talk podiums</li>
  <li>45 minute slot for giving a Free software related talk</li>
</ul>
