<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- [%
# vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
# $Id: page.tt 1177 2009-07-24 11:26:39Z holger $
%]
-->
<html xmlns="http://www.w3.org/1999/xhtml"
  xml:lang="[% lang %]" lang="[% lang %]">
  <head>
    <title>[% t.head.title %] - [% template.title %]</title>
    <meta name="keywords" content="[% t.head.keywords %]" />
    <meta name="description" content="[% t.head.description %]" />
    <meta name="Content-Language" content="[% lang %]" />
    <meta name="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index,follow,noarchive" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/default.css" type="text/css" />
    <link rel="stylesheet" href="/print.css" type="text/css" media="print" />
    <script type="text/javascript" src="/script.js"></script>
  </head>
  <body lang="[% lang %]">
    <script type="text/javascript">
      //<![CDATA[ <!--
      function setPrintStyle() {
        var i, a, main;
        for(i = 0; (a = document.getElementsByTagName("link")[i]); i++) {
          if(a.getAttribute("rel").indexOf("style") != -1) {
            a.disabled = true;
            if(a.getAttribute("media") && a.getAttribute("media") == "print") {
              a.setAttribute("media", "screen");
              a.disabled = false;
            }
          }
        }
      }
      //--> ]]>
    </script>
    <div id="pagec">
    <div id="header">
        <ul class="info">
          <li><a href="javascript:setPrintStyle()" accesskey="p" 
            title="[% t.menu.print.title %] (Access key P)"
            >[% t.menu.print.text %]</a></li>
          [% IF otherlang %]
          <li><a href="/[% "${basename}.${otherlang}" %]" accesskey="l"
            title="[% t.menu.togglelang.title %] (Access key L)"
            >[% t.menu.togglelang.text %]</a></li>
          [% END %]
        </ul>
      <div class="column-in">
        <h1 class="logo">
          <a href="/" title="[% t.h1.title %]">[% t.h1.text %]</a>
        </h1>
        <ul class="menu">
          <li><a href="/index.xhtml[% langext %]" accesskey="1"
            title="[% t.menu.home.title %]"
            >[% t.menu.home.text %]</a></li>
          <li><a href="/about.xhtml[% langext %]"
            title="[% t.leftmenu.about %]"
            >[% t.leftmenu.about %]</a></li>
          <li><a href="/contact.xhtml[% langext %]" accesskey="2"
            title="[% t.menu.contact.title %]"
            >[% t.menu.contact.text %]</a></li>
          <li><a href="/sponsorship.xhtml[% langext %]" accesskey="3"
            title="[% t.menu.spcontact.title %]"
            >[% t.menu.spcontact.text %]</a></li>
          <li><a href="http://blog.debconf.org/blog/" accesskey="4"
            title="[% t.menu.blog.title %]"
            >[% t.menu.blog.text %]</a></li>
          <li><a href="http://www.debconf.org/" accesskey="5"
            title="[% t.menu.mainsite.title %]"
            >[% t.menu.mainsite.text %]</a></li>
     	  <li><a href="/register.xhtml[% langext %]" accesskey="6"
	    title="[% t.menu.registration.title %]"
            >[% t.menu.registration.text %]</a></li>
	  <li><a href="/faq.xhtml[% langext %]">[% t.leftmenu.faq %]</a></li>
        </ul>
      </div>
    </div>
    <div id="main1">
      <div id="main2">
        <div id="left">
          <div class="column-in">
            <p class="hl1">[% t.leftmenu.confinfo %]</p>
            <ul class="nav">
             <li><a href="/index.xhtml[% langext %]"
                >[% t.menu.home.text %]</a></li>
             <li><a href="/about.xhtml[% langext %]"
                >[% t.leftmenu.about %]</a></li>
<!--               <li><a href="/https://penta.debconf.org/dc11_schedule/"
                >[% t.leftmenu.schedule %]</a></li>
              <li><a href="/http://wiki.debconf.org/wiki/DebConf11/Streams"
                >[% t.leftmenu.video %]</a></li>
-->
              <li><a href="/debianday.xhtml[% langext %]"
                >[% t.leftmenu.debianday %]</a></li>
              <li><a href="/register.xhtml[% langext %]"
                >[% t.leftmenu.register %]</a></li>
             <li><a href="/cfp.xhtml[% langext %]"
                >[% t.leftmenu.cfp %]</a></li>
              <li><a href="http://penta.debconf.org/dc11_schedule/index.en.html"
                >[% t.leftmenu.schedule %]</a></li>
              <li><a href="watch.xhtml"
                >[% t.leftmenu.watch %]</a></li>
            </ul>
            <p class="hl3">[% t.leftmenu.info %]</p>
            <ul class="nav">
              <li><a href="/dates.xhtml[% langext %]"
                >[% t.leftmenu.dates %]</a></li>
              <li><a href="/venue.xhtml[% langext %]"
                >[% t.leftmenu.venue %]</a></li>
              <li><a href="/accommodation.xhtml[% langext %]"
                >[% t.leftmenu.accommodation %]</a></li>
              <li><a href="/visas.xhtml[% langext %]"
                >[% t.leftmenu.visas %]</a></li>
              <li><a href="/payments.xhtml[% langext %]"
                >[% t.leftmenu.payments %]</a></li>
              <li><a href="/faq.xhtml[% langext %]"
                >[% t.leftmenu.faq %]</a></li>
              <!-- li><a href="/corporate.xhtml[% langext %]"
                >[% t.leftmenu.corporate %]</a></li -->
              <li><a href="/documentation.xhtml[% langext %]"
                >[% t.leftmenu.documentation %]</a></li>
            </ul>

	<p class="hl4">[% t.leftmenu.pract %]</p>
	<ul class="nav">
              <li><a href="/travel.xhtml[% langext %]"
                >[% t.leftmenu.travel %]</a></li>
              <li><a href="/contact.xhtml[% langext %]"
                >[% t.menu.contact.title %]</a></li>
              <li><a href="/map/index.xhtml[% langext %]"
                >[% t.leftmenu.map %]</a></li>
              <li><a href="/practicalinfo.xhtml[% langext %]"
                >[% t.leftmenu.practicalinfo %]</a></li>
              <li><a href="/helpus.xhtml[% langext %]"
                >[% t.leftmenu.helpus %]</a></li>
   	</ul>
            <p class="hl5">[% t.leftmenu.pastdebconfs %]</p>
            <ul class="nav">
              <li><a href="http://debconf10.debconf.org/">DebConf10</a></li>
              <li><a href="http://debconf9.debconf.org/">DebConf9</a></li>
              <li><a href="http://debconf8.debconf.org/">DebConf8</a></li>
              <li><a href="http://debconf7.debconf.org/">DebConf7</a></li>
              <li><a href="http://debconf6.debconf.org/">DebConf6</a></li>
              <li><a href="http://debconf5.debconf.org/">DebConf5</a></li>
              <li><a href="http://debconf4.debconf.org/">DebConf4</a></li>
              <li><a href="http://debconf3.debconf.org/">DebConf3</a></li>
              <li><a href="http://debconf2.debconf.org/">DebConf2</a></li>
              <li><a href="http://debconf1.debconf.org/">DebConf1</a></li>
              <li><a href="http://debconf0.debconf.org/">DebConf0</a></li>
            </ul>
            <p class="hl6">[% t.sponsors.permanent %]</p>
            <div class="sponsor">
              <a href='http://www.bytemark.co.uk/r/debian'
                title='Bytemark hosting'><img
                src="[% splogos %]/permanent/bytemark.png"
                alt='Bytemark hosting' /></a>
            </div>
            <div class="sponsor">
              <a href='http://www.man-da.de/'
                title='man-da'><img
                src="[% splogos %]/permanent/manda.png"
                alt='man-da' /></a>
            </div>
            <div class="sponsor">
              <a href='http://www.rackspace.com/'
                title='Rackspace'><img
                src="[% splogos %]/permanent/rackspace.png"
                alt='Rackspace' /></a>
            </div>
            <div class="sponsor">
              <a href='http://www.gandi.net/'
                title='gandi.net'><img
                src="[% splogos %]/permanent/gandi.png"
                alt='gandi.net' /></a>
            </div>
            <div class="sponsor">
              <a href='http://www.linode.com/r-debconf/'
                title='linode.com'><img
                src="[% splogos %]/permanent/LinodeLogoUnder.png"
                alt='linode.com' /></a>
            </div>
	<p class="hl6">[% t.sponsors.media %]</p>
	<!-- Linux Magazine -->
            <div class="sponsor">
              <a href='http://www.linux-magazine.com/'
                title='Linux Magazine'><img
                src="[% splogos %]/permanent/linuxmagazine.png"
                alt='Linux Magazine' /></a>
              <div class="textlinks"><a href="http://www.linux-magazin.de"
                  >Germany</a>,
                <a href="http://www.linux-magazine.es">Spain</a>,<br/>
                <a href="http://www.linux-magazine.pl">Poland</a>,
                <a href="http://www.linux-magazine.com.br">Brazil</a></div>
            </div>
	<!-- Linux Journal -->
	    <div class="sponsor">
              <a href='http://www.linuxjournal.com/subscribe'
                title='Linux Journal'><img
                src="[% splogos %]/media/linuxjournal.png"
                alt='Linux Journal' /></a>
            </div>

          </div>
        </div>
        <div id="right">
          <div class="column-in">
            <p class="hl6">[% t.sponsors.become %]</p>
            <div class="sponsor become-a-sponsor">
              <a href="/sponsorship.xhtml[% langext %]"
                  title="[% t.menu.spcontact.title %]"
                ><img src="[% splogos %]/sponsorship.png"
                      alt="Become our sponsor!" /></a>
            </div>
            [% # PLATINUM SPONSORS %]
	    <p class="hl6">[% t.sponsors.cat1 %]</p>
	<!-- The RS Government -->
	    <div class="sponsor"> 
              <a href='http://vladars.net/'
                title='RS Government'><img
                src="http://media.debconf.org/dc11/sponsorlogos/platinum/government.png"
                alt='RS Government' /></a> 
            </div> 

            [% # GOLD SPONSORS (none) %]
	    <p class="hl6">[% t.sponsors.cat2 %]</p>
	<!-- MTel -->
	    <div class="sponsor"> 
              <a href='http://mtel.ba/'
                title='m:tel'><img
                src="http://media.debconf.org/dc11/sponsorlogos/gold/mtel.png"
                alt='m:tel' /></a> 
            </div> 

            [% # SILVER SPONSORS %]
	<!-- Google -->
 	    <p class="hl6">[% t.sponsors.cat3 %]</p>
	    <div class="sponsor"> 
              <a href='http://www.google.com/'
                title='Google'><img
                src="http://media.debconf.org/dc11/sponsorlogos/silver/google.png"
                alt='Google' /></a> 
            </div>
	<!-- Canonical -->
	    <div class="sponsor"> 
              <a href='http://www.canonical.com/'
                title='Canonical'><img
                src="http://media.debconf.org/dc11/sponsorlogos/silver/canonical.png"
                alt='Canonical' /></a> 
            </div> 
	<!-- HP -->
	    <div class="sponsor"> 
              <a href='http://www.hp.com/'
                title='HP'><img
                src="http://media.debconf.org/dc11/sponsorlogos/silver/hp.png"
                alt='HP' /></a> 
            </div> 
	<!-- LINARO -->
	    <div class="sponsor"> 
              <a href='http://www.linaro.org/'
                title='Linaro'><img
                src="http://media.debconf.org/dc11/sponsorlogos/silver/linaro.png"
                alt='Linaro' /></a> 
            </div>

            [% # BRONZE SPONSORS %]
            <p class="hl6">[% t.sponsors.cat4 %]</p>
	<!-- Matanel -->
	    <div class="sponsor"> 
                <img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/matanel.png"
                alt='Matanel' />
        <!-- either this is disabled or it is included in the screen estate
div class="textlinks"><a href="http://www.matanel.org/">With the encouragements<br />of the Matanel Foundation</a></div -->
            </div>
	<!-- Profit Bricks -->
	    <div class="sponsor"> 
              <a href='http://www.profitbricks.com/'
                title='Profit Bricks'><img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/profitbricks.png"
                alt='Profit Bricks' /></a> 
            </div>
	<!-- Credativ -->
	    <div class="sponsor"> 
              <a href='http://www.credativ.com/'
                title='Credativ'><img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/credativ.png"
                alt='Credativ' /></a> 
            </div>
	<!-- ARM -->
	    <div class="sponsor"> 
              <a href='http://www.arm.com/'
                title='ARM'><img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/arm.png"
                alt='ARM' /></a> 
            </div>
	<!-- Renesas -->
	    <div class="sponsor"> 
              <a href='http://www.renesas.com/'
                title='Renesas'><img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/renesas.png"
                alt='Renesas' /></a> 
            </div>
	<!-- TK -->
	    <div class="sponsor"> 
              <a href='http://www.thomas-krenn.com/'
                title='Thomas Krennag'><img
                src="http://media.debconf.org/dc11/sponsorlogos/bronze/thomaskrennag.png"
                alt='Thomas Krennag' /></a> 
            </div>

            [% # STEEL SPONSORS %]
            <p class="hl6">[% t.sponsors.cat5 %]</p>
	<!-- WAVECON -->
	    <div class="sponsor"> 
              <a href='https://www.wavecon.de/'
                title='WAVECON'><img
                src="http://media.debconf.org/dc11/sponsorlogos/steel/wavecon.png"
                alt='WAVECON' /></a> 
            </div>
	<!-- Gestman, no website -->
	    <div class="sponsor"> 
              <a
                title='Gestman'><img
                src="http://media.debconf.org/dc11/sponsorlogos/steel/gestman.png"
                alt='Gestman' /></a> 
            </div>
	<!-- Univention -->
	    <div class="sponsor"> 
                <a href="http://www.univention.com/"
                title='Univention'><img
                src="http://media.debconf.org/dc11/sponsorlogos/steel/univention.png"
                alt='Univention' /></a>
            </div>
	<!-- Brandorr Group -->
	    <div class="sponsor"> 
                <a href="http://www.brandorr.com/"
                title='Brandorr Group'><img
                src="http://media.debconf.org/dc11/sponsorlogos/steel/brandorr.png"
                alt='Brandorr Group' /></a>
            </div>

            [% # SUPPORT %]
            <p class="hl6">[% t.sponsors.other %]</p>
	<!-- FFIS -->
	    <div class="sponsor"> 
              <a href='http://www.ffis.de/'
                title='FFIS'><img
                src="http://media.debconf.org/dc11/sponsorlogos/support/ffis.png"
                alt='FFIS' /></a> 
            </div> 
	<!-- SPI -->
            <div class="sponsor"> 
              <a href='http://www.spi-inc.org/'
                title='SPI'><img
                src="http://media.debconf.org/dc11/sponsorlogos/support/spi.png"
                alt='SPI' /></a> 
            </div> 
	<!-- DIVA -->
            <div class="sponsor"> 
              <a href='http://diva.ba/'
                title='DIVA'><img
                src="http://media.debconf.org/dc11/sponsorlogos/support/diva.png"
                alt='DIVA' /></a> 
            </div> 
	<!-- SARNET -->
            <div class="sponsor"> 
              <a href='#'
                title='SARnet'><img
                src="http://media.debconf.org/dc11/sponsorlogos/support/sarnet.png"
                alt='SARnet' /></a> 
            </div> 
	<!-- Dunav Insurance -->
            <div class="sponsor"> 
              <a href='http://www.dunav.com/'
                title='Dunav Insurance'><img
                src="http://media.debconf.org/dc11/sponsorlogos/support/dunavinsurance.png"
                alt='Dunav Insurance' /></a> 
            </div> 
            
          </div>
        </div>
        <div id="middle">
          <div class="column-in">
            [% PROCESS $template %]
          </div>
        </div>
        <div class="cleaner">&nbsp;</div>
      </div>
    </div>
    <div id="footer">
      <div class="column-in">
        <p class="copy">DebConf11 template by
        <a href="http://www.jabz.info/contact/jonas-jared-jacek/"
          title="Profile of Jonas Jared Jacek">Jonas Jacek</a>
        is licensed under a <a rel="license"
          href="http://creativecommons.org/licenses/by-sa/3.0/"
          >Creative Commons Attribution-Share Alike 3.0 Unported License</a>.
        <a href="http://www.jabz.org/"
          title="Jabz™ is supporting Open-Source Software and Common Good
          Initiatives">Support free software</a>!
        DebConf11 logo by Aurélio A. Heckert is licensed under a <a rel="license"
          href="http://www.gnu.org/licenses/gpl-2.0.html" >GPLv2</a> license.
        </p>
      </div>
    </div>
    </div>
  </body>
</html>
