[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Making a payment for DebConf11"
  lang = "en"
%]

<h2>Payments and Donations for DebConf11</h2>

<p align="justify">There are two organizations which can accept payment and donations for DebConf11, FFIS in the European Union and SPI in the United States. Either can be used to pay attendee fees. Other donations are encouraged, and can be paid via the same mechanisms.</p>

<p align="justify">Since we will be spending money in Euros, direct payment in Euros to FFIS will minimize bank and currency exchange fees for DebConf.</p>

<p>If you have any doubts regarding how much you need to pay, please mail <a href="mailto:registration@debconf.org">registration@debconf.org</a>.</p>


<h3>FFIS - Germany (for EUR bank transfers)</h3>

<p align="justify">FFIS can accept direct SWIFT bank transfers in euros from most European banks. When making a transfer, please indicate in the comment field that it is an attendee fee/donation for DebConf11 and the name(s) of the attendee(s).  Preferred messages are "<i>DebConf11 attendee payment for &lt;name&gt;</i>" or "<i>DebConf11 general donation</i>".    This is the information you will need:</p>

<p align="justify">
Bank identifier code (BLZ): 280 501 00
<br />Bank: Landessparkasse zu Oldenburg
<br />Account number: 10126407

<br />SWIFT: BRLA DE 22
<br />BIC: BRLA DE 21 LZO
<br />IBAN: DE84 2805 0100 0010 1264 07

<br />The bank is located in Oldenburg, Germany.</p>

<p align="justify">For more information, please see the FFIS <a href="http://www.ffis.de/Verein/donations.html">donation website</a>. The FFIS treasurer can be contacted to arrange other payment methods.</p>


<h3>SPI - USA (for USD credit-card transactions)</h3>

<p align="justify">SPI can be paid via credit card <a href="https://co.clickandpledge.com/advanced/default.aspx?wid=41803">using Click&amp;Pledge</a>. There are two forms, one for attendee fees and one for donations.  <i>Click&amp;Pledge</i> can be used to make payments worldwide in USD via VISA, MasterCard or Discover. Payments via AMEX are also accepted via our <a href="payments-amex.xhtml">alternate payment processor</a>. If you are not paying in USD and it is convenient, please consider paying to FFIS to minimize DebConf's money handling fees. If the name on the card does not match the attendee name, or you are paying for multiple attendees, please email <a href="mailto:registration@debconf.org">registration@debconf.org</a>.</p>

<p align="justify">If paying via SPI, please pay 650 USD for professional
and 1400 USD for corporate registration.</p>

<p align="justify">The <a href="mailto:treasurer@spi-inc.org">SPI Treasurer</a> can be contacted to arrange other payment methods.</p>

<!-- Hide
<h3>DIVA - BiH</h3>

<p align="justify">DIVA can accept payments in all currencies. DIVA is a NGO which handles DebConf11 money and can accept attendee payments and donations.</p>

<p align="justify">
<b>Intermediary Bank:</b>
<br />KOBBR.SBG
<br />Komercijalna Banka AD Beograd
<br />Republika Srbija
</p>
<p align="justify">
<b>Account with:</b>
<br />/100-9363078-00
<br />KOBBBA22
<br />Komercijalna Banka AD Banja Luka,
<br />Banja Luka,
<br />Bosna i Hercegovina
</p>
<p align="justify">
<b>Beneficiary:</b>
<br />IBAN: BA395710070800011203
<br />Udruzenje "DIVA" Tuzla
<br />Tuzlankse Brigade 2
</p>

<br />The bank is located in Banja Luka, Republika Srpska, Bosnia and Herzegovina.</p>
-->
