[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Registration"
  lang = "en"
%]

<!-- Registration -->
<h2>Registration for DebConf11</h2>

<p align="justify">To register for DebConf11, first <b><a href="https://penta.debconf.org/penta/user">create an account</a></b> (accounts from previous years are still valid), and then <b><a href="https://penta.debconf.org/penta/submission/dc11/person">register for DebConf11</a></b>.</p>

<p align="justify">For any questions related to registration please read the <b><a href="/faq.xhtml.en">FAQ</a></b> or mail <b><a href="mailto:registration@debconf.org">registration@debconf.org</a></b>. Please see <b><a href="/visas.xhtml">the visa page</a></b> for information on visas.</p>

<p align="justify">Registration for DebConf11 opened on <b>8th April</b>, and the deadline for sponsored food/accommodation is <b>19th May, 2011</b>. Basic registration for the conference itself will not close.</p> 

<!-- Instructions -->
<h3>Registration Instructions</h3>

<p align="justify">After logging in, please fill in at least the 'General', 'Contact' and 'Travel' tabs reachable through the 'Registration details' button on the bar on the left of the page. Please pay special attention to the 'DebConf' and 'DebCamp' boxes on the 'General' tab. <b>You need to select at least the 'I want to attend this conference' checkbox or your registration won't be valid.</b></p>

<p align="justify">There are four categories for registration for DebConf11:</p> 

<ul>
  <li><b>Sponsored registration</b> is the category which includes sponsored food and accommodation.  You must register for this category by <b>19h May, 2011</b>. In this category, you may still help cover the cost of your attendance by making a donation towards the cost of accommodation or pay for your own food tickets on-site. We will attempt to offer sponsored registration to all interested individuals who request it.<br /></li>
  <li><b>Basic registration</b> includes attendance at the conference. However, you must arrange for accommodations yourself, and pay for your own food on-site.<br /></li>
  <li><b>Professional registration</b> includes DebConf attendance for the entire week, and one week's worth of food and accommodation in our hotels included. The cost of this registration is 450 €. This category will close once we run out of rooms to assign.<br /></li>
  <li><b>Corporate registration</b> is like "Professional registration only" for those who would like to support DebConf even more, perhaps by a company or university sponsoring the registration fee. The cost of this registration is 1000 €. This category will close once we run out of rooms to assign.<br /></li>
</ul> 

<p align="justify">There is no automatic email confirmation, but you will receive an email confirmation before DebConf. If you close the registration page, and reopen it, you can verify your registration. Please do this and verify your data as our conference management system has been known to improperly save sometimes.</p>

<p align="justify">If you would like to make an additional donation to DebConf, follow the instructions at the <b><a href="/payments.xhtml">the payments page</a></b>. You may request an invoice for this, and the invoice can be written for either a donation, or a professional registration fee.</p>

<p align="justify">If you would like an invoice for any part of a fee or donation made, please mail <b><a href="mailto:registration@debconf.org">registration@debconf.org</a></b> with:</p>
<ul>
  <li>Names of attendee(s) and registration categories</li>
  <li>Organisation and address to be invoiced</li>
  <li>Whether you need a FFIS (Euro) or SPI (USD) invoice</li>
  <li>List of payments/donations made and how you would like them recorded.</li>
</ul>

<!-- DebCamp Reg -->
<h3>DebCamp</h3> 

<p align="justify">DebCamp is a smaller, less formal event than the main conference, giving an opportunity for group work on Debian projects. It will again be held in the week before DebConf, from Sunday-Saturday, <b>17th to 23rd July</b>, 2011. In an effort to make DebCamp a little more organized we ask everyone who wants to attend DebCamp to fill in an additional text field describing their work plans for the Camp.</p>

<p align="justify">If you would like  sponsorship for DebCamp, you must fill out a Debian-related workplan and the appropriate field.</p>

<p align="justify">Professional and Corporate attendees are charged their registration fee again for DebCamp (450€ / 1000€).  For example, a professional attendee attending both DebCamp and DebConf would be charged 900 € total for the two weeks.  If you would like to be sponsored for DebCamp instead, please mail us.</p>

<!-- Travel sponsorship -->
<h3>Travel sponsorship</h3> 

<p align="justify">We hope to obtain enough sponsorship to help some people attend who would not be able to pay for their own travel. We hope to allocate travel sponsorship money between April and June. We will make travel sponsorship reimbursements as soon as possible, but unfortunately, in the past this reimbursement has sometimes only been possible quite long after DebConf.</p>

<p align="justify">If you would like to apply for travel sponsorship, please fill in as much information as you can on the all registration pages. Even if you have not arranged your travel when you apply for sponsorship, we need to know which days you intend to spend at the conference. You apply for travel sponsorship on the "Travel" tab.</p>

<p align="justify">The deadline for sponsorship applications is <b>8th May, 2011</b>.</p> 


<!-- Additional info -->
<h3>Additional information</h3>

<p align="justify">If you require accommodations for a disability, or have any other needs we can help with, please mail us at <b><a href="mailto:registration@debconf.org">registration@debconf.org</a></b>.</p>

<p align="justify">DebConf is committed to a safe environment for all participants. All attendees are expected to treat all people and facilities with respect and help create a welcoming environment. If you notice behavior that fails to meet this standard, please speak up and help keep DebConf as respectful as we expect it to be.</p>

<p align="justify">If you are harassed and requests to stop are not successful, or notice a disrespectful environment, the organizers want to help. Please contact us at <b><a href="mailto:antiharassment@debian.org?Subject=DebConf">antiharassment@debian.org</a></b>. We will treat your request with dignity and confidentiality, investigate, and take whatever actions appropriate. We can provide information on security, emergency services, transportation, alternative accommodations, or whatever else may be necessary. If mediation is not successful, DebConf reserves the right to to take action against those who do not cease unacceptable behavior.</p>
