#!/usr/bin/ruby
# -*- coding: utf-8 -*-
people = {
  "Eduardo Rosales" => "I don`t have a laptop, I can`t aford one, and always borrow one for a local and regional events 
",
  "Zlatan Todoric" => "On DebConf11 I am using my friends lap. It would help in studying and in my NM process, especially combining those two in future.
",
  "Jana Wisniowska" => "for pr, demo and educative purpese. i'm giving/planning talks/workshops for beginner and advanced beginner. there are offen people comming without laptops or with a \"wrong\" os. i also work in booth on events such as linuxtag. we bring our own laptops and show just some applications, ususally don't let people realy work with. and the performance of live cd is often quite low. so i'm tired to say: but if u install it on your disk, it will work fluently ;)'
",
"Damyan Ivanov" => "The laptop would be for my son, who is 13 years old. We had the unpleasant experience to lose both mine and his laptop in a stolen bag, just before DebConf11. And while my company quickly intervened and here I am with a new laptop, I can't do the same for him. So, please. The kid would jump to the sky, really :) I promise to wipe out the present evil OS and mentor him while he installs his first copy of Debian (the old laptop was set up by me and was running also Debian).
",
"Nemanja Krecelj" => "I need it, for many reasons :) I don't have mini laptop, have one 17\" and its my Desktop PC. I am here from Banja Luka, and it will be beautiful memory of DebConf. :)
",
"Edin Cenanovic" => " I need the mini laptop for school. I have only a PC, and I can't afford myself a mini laptop. That would help me a lot at school, especially becouse I study IT.
",
"Blarson" => " I could use a portable machine that isn't falling apart with a dieing battery.
",
"Safir Secerovic" => " I do not have a computer at all and would use this netbook for developing Debian.
",
"Rene Mayorga" => " I have being working with my old AcerOne netbook for 3 years, so far it looks like the HD will fail soon, I don't have any other _personal_ computer, just the one proided by $JOB, and on that computer I don't do any Debian work, so It will be nice to have a new netbook to replace this one :)
",
"Jose Antonio Quevedo" => " My netbook's fan is failing so this HP would solve this issue. But I'm also worried because of being hearing voices commanding me to purge the virusInside.
",
"Aleksandar Milic" => "Currently I have an old laptop. So should we welcome to get better one for study and programming. 
",
"Milan Knezevic" => " Can't explain, I JUST NEED IT! 
",
"Mladen Markovic" => " I need this laptop because I can't  carry my Desktop anymore.
",
"Gunnar Wolf" => " This year my laptop was stolen. I have the minilaptop I brought to DebConf, but its short battery and dying keyboard are driving me nuts. I intended to buy a computer after returning, but my airplane ticket was over US$1300 more expensive than what I expected to spend :-/ I would really prefer a \"real\" (full, larger) laptop, but this can surely help.
",
"Bojana Borkovic" => " I don't have my own laptop, I have to share current one with my brother and sister (who are using it 24/7), so it would be really nice to actually have a laptop/netbook which I actually *own*."}

names = people.keys.sort
winner = names[rand names.size]

puts 'DebConf11 HP Laptop Giveaway!'
sleep 2
puts '* YAY *'
sleep 2
puts 'Ok, here we go. I know about %d people:' % people.size
sleep 2
puts 'Their names are: '
names.sort.each {|p| puts '- %-20s  %s...' % [p, people[p].strip[0..30].chomp]; sleep 0.3}
sleep 5
puts 'And the laptop goes to...'
sleep 3
puts '          =============================='
puts '    * * * %s  ! ! !' % winner
puts '          =============================='
puts 'The full stated reason:'
puts people[winner]
