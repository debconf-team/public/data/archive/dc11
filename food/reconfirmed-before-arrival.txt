Sat Jul 16 16:08:39 2011

Assuming everyone wants meals on the first day and no one on their
last day.  Clearly, this will be a bit wrong on both ends.


====================
Statistics for all people
Number of people: 379
Total person-days: 2843
Assuming meals per day: 2
Total number of meals: 5686
Total cost: 42645.0
Regular:    276
Vegetarian: 41
Vegan:      3
Other:      3
NotWithUs:  50

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)    40    10     1     0      51         0
2011-07-18(Mon)    53    11     1     1      66         0
2011-07-19(Tue)    61    11     1     1      74         0
2011-07-20(Wed)    70    12     1     1      84         0
2011-07-21(Thu)    80    12     1     1      94         0
2011-07-22(Fri)    94    15     1     1     111         0
2011-07-23(Sat)   206    25     3     3     237        34
2011-07-24(Sun)   259    40     3     3     305        46
2011-07-25(Mon)   263    40     3     3     309        47
2011-07-26(Tue)   262    39     3     3     307        48
2011-07-27(Wed)   264    40     3     3     310        48
2011-07-28(Thu)   262    37     3     3     305        48
2011-07-29(Fri)   257    36     3     3     299        47
2011-07-30(Sat)   220    28     2     3     253        35
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for people who requested accommodations
Number of people: 246
Total person-days: 2266
Assuming meals per day: 2
Total number of meals: 4532
Total cost: 33990.0
Regular:    204
Vegetarian: 37
Vegan:      3
Other:      1
NotWithUs:  1

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)    34    10     1     0      45         0
2011-07-18(Mon)    47    11     1     1      60         0
2011-07-19(Tue)    55    11     1     1      68         0
2011-07-20(Wed)    64    12     1     1      78         0
2011-07-21(Thu)    74    12     1     1      88         0
2011-07-22(Fri)    87    15     1     1     104         0
2011-07-23(Sat)   159    25     3     1     188         1
2011-07-24(Sun)   197    37     3     1     238         1
2011-07-25(Mon)   199    37     3     1     240         1
2011-07-26(Tue)   200    36     3     1     240         1
2011-07-27(Wed)   201    36     3     1     241         1
2011-07-28(Thu)   199    35     3     1     238         1
2011-07-29(Fri)   196    34     3     1     234         1
2011-07-30(Sat)   168    27     2     1     198         1
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for ONLY people who have both dates in Penta
Number of people: 307
Total person-days: 2473
Assuming meals per day: 2
Total number of meals: 4946
Total cost: 37095.0
Regular:    237
Vegetarian: 41
Vegan:      3
Other:      1
NotWithUs:  25

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)    37    10     1     0      48         0
2011-07-18(Mon)    50    11     1     1      63         0
2011-07-19(Tue)    58    11     1     1      71         0
2011-07-20(Wed)    67    12     1     1      81         0
2011-07-21(Thu)    77    12     1     1      91         0
2011-07-22(Fri)    91    15     1     1     108         0
2011-07-23(Sat)   168    25     3     1     197         9
2011-07-24(Sun)   221    40     3     1     265        21
2011-07-25(Mon)   225    40     3     1     269        22
2011-07-26(Tue)   224    39     3     1     267        23
2011-07-27(Wed)   226    40     3     1     270        23
2011-07-28(Thu)   224    37     3     1     265        23
2011-07-29(Fri)   219    36     3     1     259        22
2011-07-30(Sat)   182    28     2     1     213        10
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for people for whom we pay for food (S,P,C)
Number of people: 250
Total person-days: 2310
Assuming meals per day: 2
Total number of meals: 4620
Total cost: 34650.0
Regular:    208
Vegetarian: 37
Vegan:      3
Other:      1
NotWithUs:  1

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)    36    10     1     0      47         0
2011-07-18(Mon)    49    11     1     1      62         0
2011-07-19(Tue)    57    11     1     1      70         0
2011-07-20(Wed)    66    12     1     1      80         0
2011-07-21(Thu)    76    12     1     1      90         0
2011-07-22(Fri)    89    15     1     1     106         0
2011-07-23(Sat)   163    25     3     1     192         1
2011-07-24(Sun)   201    37     3     1     242         1
2011-07-25(Mon)   203    37     3     1     244         1
2011-07-26(Tue)   204    36     3     1     244         1
2011-07-27(Wed)   205    36     3     1     245         1
2011-07-28(Thu)   203    35     3     1     242         1
2011-07-29(Fri)   200    34     3     1     238         1
2011-07-30(Sat)   172    27     2     1     202         1
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for (food) sponsored people (S)
Number of people: 213
Total person-days: 2017
Assuming meals per day: 2
Total number of meals: 4034
Total cost: 30255.0
Regular:    175
Vegetarian: 33
Vegan:      3
Other:      1
NotWithUs:  1

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)    32     9     1     0      42         0
2011-07-18(Mon)    45    10     1     1      57         0
2011-07-19(Tue)    52    10     1     1      64         0
2011-07-20(Wed)    60    11     1     1      73         0
2011-07-21(Thu)    70    11     1     1      83         0
2011-07-22(Fri)    81    14     1     1      97         0
2011-07-23(Sat)   144    23     3     1     171         1
2011-07-24(Sun)   173    33     3     1     210         1
2011-07-25(Mon)   173    33     3     1     210         1
2011-07-26(Tue)   174    32     3     1     210         1
2011-07-27(Wed)   174    32     3     1     210         1
2011-07-28(Thu)   172    31     3     1     207         1
2011-07-29(Fri)   170    30     3     1     204         1
2011-07-30(Sat)   146    24     2     1     173         1
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for (food) UNsponsored people  (B,None)
Number of people: 128
Total person-days: 525
Assuming meals per day: 2
Total number of meals: 1050
Total cost: 7875.0
Regular:    67
Vegetarian: 4
Vegan:      0
Other:      2
NotWithUs:  49

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)     4     0     0     0       4         0
2011-07-18(Mon)     4     0     0     0       4         0
2011-07-19(Tue)     4     0     0     0       4         0
2011-07-20(Wed)     4     0     0     0       4         0
2011-07-21(Thu)     4     0     0     0       4         0
2011-07-22(Fri)     5     0     0     0       5         0
2011-07-23(Sat)    42     0     0     2      44        33
2011-07-24(Sun)    57     3     0     2      62        45
2011-07-25(Mon)    59     3     0     2      64        46
2011-07-26(Tue)    57     3     0     2      62        47
2011-07-27(Wed)    58     4     0     2      64        47
2011-07-28(Thu)    58     2     0     2      62        47
2011-07-29(Fri)    56     2     0     2      60        46
2011-07-30(Sat)    47     1     0     2      50        34
2011-07-31(Sun)     0     0     0     0       0         0



====================
Statistics for Corp/Prof attendees (C,P)
Number of people: 37
Total person-days: 293
Assuming meals per day: 2
Total number of meals: 586
Total cost: 4395.0
Regular:    33
Vegetarian: 4
Vegan:      0
Other:      0
NotWithUs:  0

                  Reg   Veg  Vegan Other   Total      None
2011-07-17(Sun)     4     1     0     0       5         0
2011-07-18(Mon)     4     1     0     0       5         0
2011-07-19(Tue)     5     1     0     0       6         0
2011-07-20(Wed)     6     1     0     0       7         0
2011-07-21(Thu)     6     1     0     0       7         0
2011-07-22(Fri)     8     1     0     0       9         0
2011-07-23(Sat)    19     2     0     0      21         0
2011-07-24(Sun)    28     4     0     0      32         0
2011-07-25(Mon)    30     4     0     0      34         0
2011-07-26(Tue)    30     4     0     0      34         0
2011-07-27(Wed)    31     4     0     0      35         0
2011-07-28(Thu)    31     4     0     0      35         0
2011-07-29(Fri)    30     4     0     0      34         0
2011-07-30(Sat)    26     3     0     0      29         0
2011-07-31(Sun)     0     0     0     0       0         0



